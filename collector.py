#!/usr/bin/env python3

from os import environ
from sys import exit
from time import sleep

import datetime
import logging

logging.basicConfig(filename='collector.log', format='%(asctime)s - %(name)s:%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger()
log_formatter = logging.Formatter('%(asctime)s - %(name)s:%(levelname)s: %(message)s')

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(log_formatter)
logger.addHandler(stream_handler)

import urllib.request
import satnogs_query

# FIXME decouple logic
import twitter
twitter_client = twitter.TwitterClient()

import ffmpeg
import tempfile
import subprocess
import shutil
from PIL import Image

def execute_function_on_tempfile(func, file_content, suffix, *args, **kwargs):
    with tempfile.NamedTemporaryFile() as tmp:
        tmp.write(file_content)
        dirpath = tempfile.mkdtemp()
        result_file = f"{dirpath}/result{suffix}"
        func(tmp.name, result_file, *args, **kwargs)
        with open(result_file, "rb") as tmp_result:
            result = tmp_result.read()
        shutil.rmtree(dirpath)
    return result


def generate_wav(audio):
    with tempfile.NamedTemporaryFile() as tmp_ogg:
        tmp_ogg.write(audio)
        i = ffmpeg.input(tmp_ogg.name)
        dirpath = tempfile.mkdtemp()
        ffmpeg.output(i.audio, f"{dirpath}/audio.wav", ar='11025').run()
        with open(f"{dirpath}/audio.wav", "rb") as tmp_wav:
            wav = tmp_wav.read()
        shutil.rmtree(dirpath)
    return wav

def scale_image(input_file, result_file, factor):
    # FIXME: Remove or test
    img = Image.open(input_file)
    img = img.resize((int(img.width * factor), int(img.height * factor)), Image.ANTIALIAS)
    img.save(result_file)


def convert_png_to_jpg(input_file, result_file):
    im = Image.open(input_file)
    rgb_im = im.convert('RGB')
    rgb_im.save(result_file)


def process_noaa_image(wav, northbound = False):
    with tempfile.NamedTemporaryFile() as tmp_wav:
        tmp_wav.write(wav)
        dirpath = tempfile.mkdtemp()
        subprocess.run(["wxtoimg", "-e", "HVC"]
                + (["-N"] if northbound else [])
                + [tmp_wav.name, f"{dirpath}/result.png"])
        with open(f"{dirpath}/result.png", "rb") as tmp_result:
            result = tmp_result.read()
        shutil.rmtree(dirpath)
    return result

def process_observation(observation, observation_data):
    waterfall_image = download(observation_data["waterfall_url"])
    data_images = [download(x) for x in observation_data["data_image_urls"]]
    audio = download(observation_data["audio_url"])
    northbound = abs(180 - observation_data["rise"]) < abs(180 - observation_data["set"])

    if observation_data["satellite_name"].startswith("NOAA"):
        logger.info("Generating wav")
        wav = generate_wav(audio)
        logger.info("Done generating wav")
        logger.info("Generating colorized image")
        colorized_image = process_noaa_image(wav, northbound)
        if len(colorized_image) > 3*10**6:
            logging.info(f"resizing image with size {len(colorized_image)}")
            colorized_image = execute_function_on_tempfile(convert_png_to_jpg, colorized_image, ".jpg")
        logger.info(f"Done colorizing image: {len(colorized_image)} Byte")
        twitter_client.handle_observation(observation, observation_data, waterfall_image, [colorized_image])
    else:
        twitter_client.handle_observation(observation, observation_data, waterfall_image, data_images)

def download(url):
    logger.info(f"Downloading: {url}")
    with urllib.request.urlopen(url) as req:
        data = req.read()
    logger.info("Download finished")
    return data

def loop(satnogs_address, station, satellites, observer):
    processed_observations = []
    start_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M")
    try:
        while True:
            for satellite in satellites:
                logger.info(f"Querying for satellite {satellite}")
                for observation in satnogs_query.get_past_station_observations(satnogs_address, station, satellite, observer, start_time = start_time):
                    if observation in processed_observations:
                        continue
                    logger.info(f"Processing new observation {observation}")
                    observation_data = satnogs_query.get_observation_data(satnogs_address, observation)
                    process_observation(observation, observation_data)
                    processed_observations.append(observation)
            sleep(30)
    except KeyboardInterrupt:
        pass
    except:
        logging.exception("Unhandled error occurred")
        exit(42)


if __name__ == "__main__":
    station = environ.get("SATNOGS_STATION", None)
    satellites_plain = environ.get("SATNOGS_SATELLITES", None)
    observer = environ.get("SATNOGS_OBSERVER", None)
    satnogs_address = environ.get("SATNOGS_ADDRESS", "https://network.satnogs.org")

    if not station or not satellites_plain:
        print("Sation and satellites to follow must be given")
        exit(1)
    
    satellites = [x.strip() for x in satellites_plain.split(" ") if x.strip()]

    loop(satnogs_address, station, satellites, observer)
