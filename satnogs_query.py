#!/usr/bin/env python3

from os import environ
from sys import exit

import logging
logger = logging.getLogger(__name__)

import re
import urllib.parse
import urllib.request

def get_past_station_observations(satnogs_address, station, satellite, observer=None, start_time=None):
    data = {
        "future": 0,
        "bad": 0,
        "unvetted": 0,
        "failed": 0,
        "norad": satellite,
        "station": station,
        "results": "d1", # only those with data
    }
    if start_time:
        data["start-time"] = start_time
    if observer:
        data["observer"] = observer

    values = urllib.parse.urlencode(data)
    url = satnogs_address + "/observations/?" + values
    logger.info(f"Querying {url}")

    with urllib.request.urlopen(url) as response:
        page = str(response.read())

    observation_ids = [m for m in re.findall(r"data-href=\"/observations/(\d+)/\"", page)]
    logger.info(f"Matched the following observations: {','.join(observation_ids)}")

    return observation_ids

def get_observation_data(satnogs_address, observation):
    logger.info(f"Fetching data for observation {observation}")
    url = f"{satnogs_address}/observations/{observation}/"
    with urllib.request.urlopen(url) as response:
        page = str(response.read())

    waterfall_image_url = re.search(f"/media/data_obs/{observation}/waterfall_{observation}_\d+-\d+-\d+T\d+-\d+-\d+.png", page).group(0)
    data_image_urls = re.findall(f"src=\"(/media/data_obs/{observation}/data_{observation}_\d+-\d+-\d+T\d+-\d+-\d+.png)", page)
    audio_url = re.search(f"data-audio=\"(/media/data_obs/{observation}/(.*?))\"", page).group(1)
    satellite_id = re.search("<a href=\"#\" data-toggle=\"modal\" data-target=\"#SatelliteModal\" data-id=\"(\d+)\">", page).group(1)
    satellite_name = re.search(f"\s+{satellite_id}\s+-\s+(.*?)</a>", page).group(1).strip()[:-2] # remove magic \n
    rise = float(re.search("<div class=\"green_circle\"></div>.*?((\d+)\.(\d+))", page).group(1))
    set_ = float(re.search("<div class=\"red_circle\"></div>.*?((\d+)\.(\d+))", page).group(1))
    data_timeframe_start = re.search(f"data-timeframe-start=\"(.*?)\"", page).group(1).strip()
    data_timeframe_end = re.search(f"data-timeframe-end=\"(.*?)\"", page).group(1).strip()

    return {
        "waterfall_url": f"{satnogs_address}{str(waterfall_image_url)}",
        "data_image_urls": [f"{satnogs_address}{data_image_url}" for data_image_url in data_image_urls],
        "audio_url": f"{satnogs_address}{audio_url}",
        "satellite_id": satellite_id,
        "satellite_name": satellite_name,
        "rise": rise,
        "set": set_,
        "data_timeframe_start": data_timeframe_start,
        "data_timeframe_end": data_timeframe_end,
        "url": url,
        "id": observation,
    }

if __name__ == "__main__":
    station = environ.get("SATNOGS_STATION", None)
    satellites_plain = environ.get("SATNOGS_SATELLITES", None)
    observer = environ.get("SATNOGS_OBSERVER", None)
    satnogs_address = environ.get("SATNOGS_ADDRESS", "https://network.satnogs.org")

    if not station or not satellites_plain:
        print("Sation and satellites to follow must be given")
        exit(1)
    
    satellites = [x.strip() for x in satellites_plain.split(" ") if x.strip()]

    logger.setLevel(20)
    for satellite in satellites:
        for observation in get_past_station_observations(satnogs_address, station, satellite, observer):
            print(get_image_urls_from_observation(satnogs_address, observation))
