#!/usr/bin/env python3

import tempfile
import time
import tweepy

import logging
logger = logging.getLogger(__name__)

from twitter_config import *

class TwitterClient:
    def __init__(self):
        global consumer_key, consumer_secret, access_token, access_token_secret
        self.auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        self.auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(self.auth)
        logger.info(f"Logged in to twitter as '{self.api.me().name}")

    def handle_observation(self, observation, observation_data, waterfall_image, data_images, tweet_waterfall = False):
        global enabled
        if not enabled:
            return
        with tempfile.TemporaryFile() as tmp:
            tmp.write(data_images[0])
            text = f"New satellite image from {observation_data['satellite_name']} at {observation_data['data_timeframe_start']} UTC. {observation_data['url']} #cccamp19"
            try:
                status = self.api.update_with_media("image.png", text, file=tmp)
            except tweepy.error.TweepError as e:
                logger.warning(f"Couldn't tweet observation {observation}: {e}")
                return
            logger.info(f"Tweeted: '{text}'")
        if tweet_waterfall:
            time.sleep(0.1)
            with tempfile.TemporaryFile() as tmp:
                tmp.write(waterfall_image)
                waterfall_status = self.api.update_with_media("image.png", in_reply_to_status_id=status.id, file=tmp)
