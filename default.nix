with import <nixpkgs> {}; {
  env = stdenv.mkDerivation {
    name = "env";
    buildInputs = [
      pipenv
      python3Packages.pillow
    ];
  };
}
